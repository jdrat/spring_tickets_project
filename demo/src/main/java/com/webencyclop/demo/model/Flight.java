package com.webencyclop.demo.model;

import lombok.Data;
import java.sql.Date;

        import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Data
@Entity
@Table(name = "flights")
public class Flight {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "flight_id")
    private int id;

    @NotNull
    @Column(name = "departure_city")
    private String departureCity;

    @NotNull(message="Email is compulsory")
    @Column(name = "arrival_city")
    private String arrivalCity;

    @NotNull(message="air is compulsory")
    @Column(name = "air_line")
    private String airLine;

    @NotNull(message="Email is compulsory")
    @Column(name = "number_of_tickets")
    private int numberOfTickets;

    @NotNull(message="Email is compulsory")
    @Column(name = "date")
    String date;

    @OneToMany(mappedBy = "flight")
    private Set<Ticket> tickets;

}
