package com.webencyclop.demo.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "ticket")
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ticket_id")
    private int id;


    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="flight_id")
    private Flight flight;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="auth_user_id")
    private User user;

}
