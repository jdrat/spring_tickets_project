package com.webencyclop.demo.controller;

import com.webencyclop.demo.model.Flight;
import com.webencyclop.demo.model.Ticket;
import com.webencyclop.demo.model.User;
import com.webencyclop.demo.service.FlightService;
import com.webencyclop.demo.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;

@Controller
public class TicketController {

    @Autowired
    TicketService ticketService;

    @Autowired
    FlightService flightService;

    @GetMapping("/tickets")
    public String showTickets() {
        ticketService.getAllTickets();
        return "home";

    }

    @PostMapping(name="/tickets")
    public ModelAndView registerUser(@Valid Ticket ticket, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        // Check for the validations
        if(bindingResult.hasErrors()) {
            modelAndView.setViewName("register");
            modelAndView.addObject("successMessage", "Please correct the errors in form!");
        }
        else {
            ticketService.saveTicket(ticket);
            modelAndView.addObject("successMessage", "User is registered successfully!");
        }
        modelAndView.addObject("ticket", new Ticket());
        modelAndView.setViewName("home");
        return modelAndView;
    }
}
