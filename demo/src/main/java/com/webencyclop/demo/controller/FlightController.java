package com.webencyclop.demo.controller;

import com.webencyclop.demo.model.Flight;

import com.webencyclop.demo.model.Ticket;
import com.webencyclop.demo.model.User;
import com.webencyclop.demo.service.FlightService;
import com.webencyclop.demo.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@Controller
public class FlightController {

    @Autowired
    FlightService flightService;

    @PostMapping(value="/admin")
    public ModelAndView registerUser(@Valid Flight flight, BindingResult bindingResult, ModelMap modelMap) {
        ModelAndView modelAndView = new ModelAndView();
        // Check for the validations
        if(bindingResult.hasErrors()) {
            modelAndView.setViewName("admin");
            modelAndView.addObject("successMessage", "Please correct the errors in form!");
            modelMap.addAttribute("bindingResult", bindingResult);
        }
        else {
            flightService.saveFlight(flight);
            modelAndView.addObject("successMessage", "Flight is added successfully!");
        }
        modelAndView.addObject("flight", new Flight());
        modelAndView.setViewName("admin");
        return modelAndView;
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET, params = {"departure", "arrival", "date"})
    public ModelAndView findFlights(@RequestParam String departure, @RequestParam String arrival, @RequestParam String date) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("foundFLights");
        List<Flight> flightSearchingResult = flightService.findFlights(departure, arrival);
        modelAndView.addObject("flights", flightSearchingResult);
        return modelAndView;
    }


}
