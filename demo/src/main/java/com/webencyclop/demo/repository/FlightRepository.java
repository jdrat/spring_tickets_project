package com.webencyclop.demo.repository;

import com.webencyclop.demo.model.Flight;
import org.apache.catalina.LifecycleState;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FlightRepository extends JpaRepository<Flight, Integer> {

    List<Flight> findByDepartureCityAndArrivalCityAllIgnoreCase(String departure, String arrival);
}
