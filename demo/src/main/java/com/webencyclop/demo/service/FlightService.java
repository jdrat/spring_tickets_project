package com.webencyclop.demo.service;

import com.webencyclop.demo.model.Flight;
import com.webencyclop.demo.repository.FlightRepository;
import com.zaxxer.hikari.util.FastList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FlightService {

    @Autowired
    FlightRepository flightRepository;

    public void saveFlight(Flight flight){
        flightRepository.save(flight);
    }

    public List<Flight> findFlights(String departure, String arrival){
        return flightRepository.findByDepartureCityAndArrivalCityAllIgnoreCase(departure, arrival);
    }
}
